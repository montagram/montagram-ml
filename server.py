import SocketServer
import json
import brainPropre


fnn = brainPropre.NetworkReader.readFrom('network.xml')
print "read network.xml"

class MyTCPServer(SocketServer.ThreadingTCPServer):
    allow_reuse_address = True

class MyTCPServerHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        try:
            data = json.loads(self.request.recv(1024).strip())
            # process the data, i.e. print it:
            # send some 'ok' back
            print data
            print data['picture']
            result = brainPropre.load_image(data['picture'], fnn)
            print result
            self.request.sendall(json.dumps({'name': result[0], 'accuracy1': int(result[1]),'accuracy2': int(result[2]), 'accuracy3': 100 - int(result[1] + result[2]) }))
        except Exception, e:
            print "Exception while receiving message: ", e


#Initialization
#error = 0
#unknown = 0
#(alldata, trndata,tstdata) = makedataset()
#(trnResultTab, tstResultTab) = training(trndata,tstdata)
#testAll(error,unknown,alldata)
#plotOverfitting(trnResultTab,tstResultTab)            
#testOneIm("mc1","MC",error,unknown)

server = MyTCPServer(('0.0.0.0', 13373), MyTCPServerHandler)
server.serve_forever()




