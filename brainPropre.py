from pybrain.datasets import ClassificationDataSet
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SoftmaxLayer
from pybrain.tools.xml.networkwriter import NetworkWriter
from pybrain.tools.xml.networkreader import NetworkReader
#from pybrain.tools.customxml import NetworkWriter, NetworkReader
from PIL import Image
import numpy as np

TAILLE = 70
ERROR_THRESHOLD = 0.80

INDEX_MP = 0
INDEX_CER = 1
INDEX_TC = 2

#Put image in grayscale, rescale it, convert it as a vector
def imageProcess(im):
	im = im.convert("L")
	im = im.resize((TAILLE,TAILLE))
	matrix = np.asarray(im)
	imVect = matrix.flatten().tolist()
	return imVect

#Test for one unknown image (from user)
def tuple_result(result):

	resultList=[]
	for i in result:
		resultList.append(i)

	mountain = max(resultList)
	mountainIndex = resultList.index(mountain)
	mountainPercent = mountain*100
	if mountain >= ERROR_THRESHOLD :
		print "on est sup a ERROR_THRESHOLD"
		if mountainIndex == INDEX_TC :
			return ('Le Batiment TC',  mountainPercent, 0, 0)
		elif mountainIndex == INDEX_CER :
			return ('le Mont Cervin', mountainPercent, 0, 0)
		elif mountainIndex == INDEX_MP :
			return ('le Huayna Picchu', mountainPercent, 0, 0)
	else:
		return ('Resultat Inconnu. (Cervin, Huayna Picchu, Batiment TC)', resultList[INDEX_CER]*100, resultList[INDEX_MP]*100, resultList[INDEX_TC]*100)

def load_url_img(img_url):
	import urllib2
	from cStringIO import StringIO
	input_file = StringIO(urllib2.urlopen(img_url).read())
	input_file.seek(0)
	pic = Image.open(input_file)
	return imageProcess(pic)

def load_image(img_url, fnn):
	print ("load image") 
	image = load_url_img(img_url)
	print("search")
	result = fnn.activate(image)
	print ("result after activate")
	return tuple_result(result)

